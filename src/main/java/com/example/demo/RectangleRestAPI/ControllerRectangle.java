package com.example.demo.RectangleRestAPI;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControllerRectangle {
    @GetMapping("/rectangle-area")
    public float getArea() {
        
        Rectangle rectangle = new Rectangle(3.0f , 4.0f);
        return (float) rectangle.getArea();
}

    @GetMapping("/rectangle-perimeter")
    public double getPerimeter() {

        Rectangle rectangle = new Rectangle(5.0f , 7.0f);
        return (float) rectangle.getPerimeter();
    }
}
